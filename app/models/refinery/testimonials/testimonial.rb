module Refinery
  module Testimonials
    class Testimonial < Refinery::Core::BaseModel
      self.table_name = 'refinery_testimonials'

      attr_accessible :name, :age, :photo_id, :address, :occupation, :hobbies, :testimonial, :position

      acts_as_indexed :fields => [:name, :address, :occupation, :hobbies, :testimonial]

      validates :name, :presence => true, :uniqueness => true

      belongs_to :photo, :class_name => '::Refinery::Image'
    end
  end
end
